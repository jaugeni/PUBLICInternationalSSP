//
//  InternationalSSPTests.swift
//  InternationalSSPTests
//
//  Created by YAUHENI IVANIUK on 4/2/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import XCTest
@testable import InternationalSSP

class intToDateTest: XCTestCase {
    
    func testIsCorrectReturn() {
        let timestamp1 = 1522722922
        let timestamp2 = 1522740369
        let answer1 = "Apr 2, 2018 at 10:35 PM"
        let answer2 = "Apr 4, 2018 at 3:26 AM"
        XCTAssertEqual(timestamp1.convertIntToStringDate(), answer1)
        XCTAssertNotEqual(timestamp2.convertIntToStringDate(), answer2)
    }

    

    
}
