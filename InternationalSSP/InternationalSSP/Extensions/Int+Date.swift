//
//  Double+Date.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/29/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import Foundation

extension Int {
    func convertIntToStringDate()-> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM d, yyyy 'at' h:mm a"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
}
