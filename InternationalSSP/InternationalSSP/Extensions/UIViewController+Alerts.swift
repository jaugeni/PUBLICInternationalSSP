//
//  UIViewController+Alerts.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/30/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import AVKit
import UIKit

extension UIViewController {
    
    enum Constants {
        static let settings = "Settings"
        static let notNow = "Not Now!"
        static let confirm = "OK"
    }

// opening the setting, when locations were denied
    func presentSettingAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: Constants.notNow, style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: Constants.settings, style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true)
    }
    
// the regular alert controller can be called across the app
    func presentAlert(message: String) {
        let alertController = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.confirm, style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
