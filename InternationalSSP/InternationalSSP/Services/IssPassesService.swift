//
//  IssPassesService.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/29/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class IssPassesService {
    
    static let share = IssPassesService()
    
    func getIssPassesArray(lat: Double, lon: Double, completion: @escaping (_ success: Bool, _ error: String?, _ issPssArray: [IssPassModel]?)->()) {
        
        let issUrl = "http://api.open-notify.org/iss-pass.json?lat=\(lat)&lon=\(lon)&n=13"
        
        print("YI location \(issUrl)")
        
        var issPassedArray = [IssPassModel]()
        
        Alamofire.request(issUrl).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                let json = JSON(data)
                let issResponseJsonArray = json["response"].arrayValue
                if issResponseJsonArray.count != 0 {
                    for issJsonObject in issResponseJsonArray {
                        let iss = JSON(issJsonObject)
                        issPassedArray.append(IssPassModel(risetime: iss["risetime"].intValue,
                                                           duration: iss["duration"].intValue))
                    }
                }
                completion(true, nil, issPassedArray)
            } else {
                if let error = response.result.error {
                    completion(false, error.localizedDescription, nil)
                }
                debugPrint(response.result.error as Any)
            }
        }
    }
}
