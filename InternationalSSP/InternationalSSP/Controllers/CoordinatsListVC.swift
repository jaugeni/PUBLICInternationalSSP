//
//  ViewController.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/28/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import UIKit
import CoreLocation

class CoordinatsListVC: UIViewController {
    
    @IBOutlet weak var latTextField: UITextField!
    @IBOutlet weak var lonTextField: UITextField!
    @IBOutlet weak var issPassTable: UITableView!
    
    var locationManager = CLLocationManager()
    
    var issPassedArray = [IssPassModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        issPassTable.delegate = self
        issPassTable.dataSource = self
        
        latTextField.delegate = self
        lonTextField.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewPlaceholder(array: issPassedArray)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //validating textField

    func validateLat(_ textField: UITextField) -> (Bool, String?) {

        guard let text = textField.text, let number = Double(text) else {
            textField.text = ""
            return (false, "Latitude should be a number!")
        }

        if number < -71 || number > 71 {
            textField.text = ""
            return ( false, "Latitude shold be between -71..71")
        }
        return (true, nil)
    }
    
    func validateLon(_ textField: UITextField) -> (Bool, String?) {
        
        guard let text = textField.text, let number = Double(text) else {
            textField.text = ""
            return (false, "Longitude should be a number!")
        }
        if number < -180 || number > 180 {
            textField.text = ""
            return(false, "Longitude shold be between -180..180")
        }
        return (true, nil)
        
    }
    
    
    func isOwnLatAndLonValid() -> Bool {
        let (validLat, messageLat) = validateLat(latTextField)
        if !validLat {
            self.presentAlert(message: messageLat!)
            return false
        }
        
        let (validLon, messageLon) = validateLon(lonTextField)
        if !validLon {
            self.presentAlert(message: messageLon!)
            return false
        }
        return true
    }
    
// tableView placeholder if no data
    
    func tableViewPlaceholder(array: [IssPassModel]){
        if array.count == 0 {
            issPassTable.tableFooterView = UIView(frame: CGRect.zero)
            issPassTable.backgroundColor = UIColor.clear
            
            let label = UILabel()
            label.frame.size.height = issPassTable.frame.size.height
            label.frame.size.width = issPassTable.frame.size.width
            label.center = issPassTable.center
            label.center.y = (issPassTable.frame.size.height/2)
            label.numberOfLines = 0
            label.textColor = UIColor.gray
            label.text = "You can check when The international space station was above your head using your current or custom coordinates."
            label.textAlignment = .center
            label.tag = 1
            
            self.issPassTable.addSubview(label)
        }
    }
    
    @IBAction func ownCoordinatesPressed(_ sender: Any) {
        view.endEditing(true)
        
        guard isOwnLatAndLonValid() else {return}
        
        guard let latDouble = Double(latTextField.text!) else {return}
        guard let lonDouble = Double(lonTextField.text!) else {return}
        
        IssPassesService.share.getIssPassesArray(lat: latDouble, lon: lonDouble) { (success, error, issPassedArray) in
            if success {
                self.issPassedArray = issPassedArray!
                self.issPassTable.reloadData()
            } else {
                self.presentAlert(message: error!)
                print(error!)
            }
        }
    }
    
    @IBAction func myCurrentLocationPressed(_ sender: Any) {
        view.endEditing(true)
        
        lonTextField.text = ""
        latTextField.text = ""
        
// asking permission for a location just when the user pressed first time my current location
        configLocationPermission()
        
        requestCoordinates { (lat, lon) in
            IssPassesService.share.getIssPassesArray(lat: lat, lon: lon, completion: { (success, error, issPassedArray) in
                if success {
                    self.issPassedArray = issPassedArray!
                    self.issPassTable.reloadData()
                } else {
                    self.presentAlert(message: error!)
                }
            })
        }
    }
}

extension CoordinatsListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if issPassedArray.count > 0 {
            self.issPassTable.viewWithTag(1)?.removeFromSuperview()
        }
        return issPassedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = issPassTable.dequeueReusableCell(withIdentifier: "passCell", for: indexPath) as? PassCell {
            cell.configureCell(message: issPassedArray[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
}

extension CoordinatsListVC: CLLocationManagerDelegate {
    
    func configLocationPermission() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted {
            let title = "Set Location Access to \"While Using the App\""
            let message = "1. Tap Sttings\n 2. Tap Location\n 3. Tap While Using the App"
            self.presentSettingAlert(title: title, message: message)
        }
    }
    
    func requestCoordinates(completion: @escaping (_ lat: Double, _ lon:Double)->()) {
        
        guard let currentCoordinates = locationManager.location else {
            print("YI: current coordinat faild!")
            return
        }
        
        completion(currentCoordinates.coordinate.latitude, currentCoordinates.coordinate.longitude)
    }
}

extension CoordinatsListVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var newText = textField.text! as NSString
        newText = newText.replacingCharacters(in: range, with: string) as NSString
        return newText.length <= 9
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

