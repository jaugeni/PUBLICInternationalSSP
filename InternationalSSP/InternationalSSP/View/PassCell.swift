//
//  PassCell.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/29/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import UIKit

class PassCell: UITableViewCell {
    
    @IBOutlet weak var infoLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(message: IssPassModel) {
        let dateStr = message.risetime.convertIntToStringDate()
        infoLbl.text = "The international space station was above your head on \(dateStr) during  \(message.duration) seconds."
    }
    
}
