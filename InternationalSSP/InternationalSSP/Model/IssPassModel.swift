//
//  IssPassModel.swift
//  InternationalSSP
//
//  Created by YAUHENI IVANIUK on 3/29/18.
//  Copyright © 2018 YAUHENI IVANIUK. All rights reserved.
//

import Foundation

struct IssPassModel {
    let risetime: Int
    let duration: Int
}
